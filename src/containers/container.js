import React, {Component, Fragment} from 'react';
import './container.css';

import {BrowserRouter as Router, Route} from "react-router-dom";
import Header from "../components/Header/Header";
import Posts from "./Posts/Posts";
import AddNewPost from "./AddNewPost/AddNewPost";
import OnePost from "./OnePost/OnePost";
import EditPost from "./EditPost/EditPost";


class Container extends Component {

    render() {
        return (
            <div className="container">
                <Router>
                    <Fragment>
                        <Header/>
                        <Route path="/"  exact component={Posts}/>
                        <Route path="/add" component={AddNewPost}/>
                        <Route path='/posts/:id' component={OnePost}/>
                        <Route path="/post/:postId" component={EditPost}/>
                    </Fragment>
                </Router>
            </div>
        );
    }
}

export default Container;