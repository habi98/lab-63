import React, {Component} from 'react';
import axios from '../../axios-blog';
import './OnePost.css'
import {NavLink} from "react-router-dom";
class OnePost extends Component {
    state = {
      onePost: null
    };

    componentDidMount() {
        axios.get(this.props.match.url + '.json')
        .then(response => {
         this.setState({onePost: response.data});

      });
    }

    deleteHandler = () => {
        axios.delete(this.props.match.url + '.json').then(() => {
            this.props.history.replace('/')
        })
    };
    render() {
        let onePost = null;
        if (this.state.onePost) {
            return <div className="onePost"><h3 className="text-title">{this.state.onePost.title}</h3>
                <p className="description">{this.state.onePost.description}</p>
                <button className="delete" onClick={this.deleteHandler}>delite</button>
                <NavLink to= {"/post/" + this.props.match.params.id + '/edit'}><button className="edit">edit</button></NavLink></div>
        }
        return (
            <div>
                {onePost}
            </div>
        );
    }
}

export default OnePost;