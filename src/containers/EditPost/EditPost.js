import React, {Component} from 'react';
import AddNewPost from "../AddNewPost/AddNewPost";

class EditPost extends Component {
    state = {
        posts: null
    };
    render() {
        let form = <AddNewPost onClick={this.postHandler} posts={this.state.posts}/>;
        return (
            <div>
                <h3 className="title">Edit Blog </h3>
                {form}
            </div>
        );
    }
}

// some test
export default EditPost;