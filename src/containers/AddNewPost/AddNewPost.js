import React, {Component} from 'react';
import './AddNewPost.css'
import axios from "../../axios-blog";

class AddNewPost extends Component {
       state = {
           title: '',
           description: ''
       };

    valueChanged = (event) => {
        const name = event.target.name;
        this.setState({[name]: event.target.value});
    };
    postHandler = () => {
        const post = {title: this.state.title, description: this.state.description};
        axios.post('/posts.json', post).then(response => {
            this.props.history.push("/")
        })
    };

    render() {
        return (
            <div className="block-form">
                <span className="span-title"> Title</span>
                <input onChange={this.valueChanged} name="title"  value={this.state.title} className="form-add" type="text"/>
                <textarea className="description" type="text" name="description"
                          onChange={this.valueChanged}
                          value={this.state.description}>a
                </textarea>
                <button type="button" className="btn-save" onClick={this.postHandler}>save</button>
            </div>
        );
    }
}

export default AddNewPost;