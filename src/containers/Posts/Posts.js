import React, {Component, Fragment} from 'react';
import './Posts.css';
import axios from "../../axios-blog";
class Posts extends Component {
    state = {
      posts: null
    };

    componentDidMount() {
        axios.get('/posts.json').then(response => {
            if(!response.data) return null;
                const posts = Object.keys(response.data).map(id => {
                return {...response.data[id], id};
                });
            this.setState({posts: posts})
        });

    }

    moreHandler = (id) => {
        this.props.history.push("/posts/" + id)
    };
    render() {
        let posts = null ;
        if(this.state.posts )  {
            posts = this.state.posts.map((post, id) => (
                <div key={id} className="post-title"><h3 className="text-title">{post.title}</h3><p className="description">{post.description}</p><button onClick={() => this.moreHandler(post.id)}  className="btn-link">read more ></button></div>
            ))
        }
        return (
         <Fragment>
             {posts}
         </Fragment>
        );
    }
}

export default Posts;