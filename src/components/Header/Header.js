import React from 'react';
import {NavLink} from "react-router-dom";
import './Header.css';
const Header = () => {
    return (
        <div className="header clearfix">
            <p  className="logo">My Blog</p>
          <ul className="nav-ul">
              <li><NavLink className="link" to="/">Home</NavLink></li>
              <li><NavLink className="link" to="/add">Add</NavLink></li>
              <li><NavLink className="link" to="/about">About</NavLink></li>
              <li><NavLink className="link" to="/contacts">Contacts</NavLink></li>

          </ul>
        </div>
    );
};

export default Header;